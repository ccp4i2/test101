from __future__ import print_function

import sys
import os
import subprocess
import tempfile
import shutil
import argparse

allTaskTestCases = {
    'import_merged':[
        ['import_merged',
            '--HKLIN', 
                'fullPath=$CCP4I2/demo_data/gamma/merged_intensities_Xe.mtz',
        ]
    ],
    'refmac_gamma':[
        ['prosmart_refmac',
            '--F_SIGF', 
                'fullPath=$CCP4I2/demo_data/gamma/merged_intensities_Xe.mtz',
            '--XYZIN', 
                'fullPath=$CCP4I2/demo_data/gamma/gamma_model.pdb',
        ]
    ],
    #<SFAC>Xe</SFAC>
    #<NTRY>100</NTRY>
    #<FIND>4</FIND>
    #<PARTIALMODELORMAP>SEARCH</PARTIALMODELORMAP>
    #<COMP_BY>ASU</COMP_BY>
    #<WAVELENGTH>1.542</WAVELENGTH>
    #<PURE_ANOMALOUS>False</PURE_ANOMALOUS>
    #<LLGC_CYCLES>50</LLGC_CYCLES>
    'phaserEP_gamma':[
        ['phaser_EP',
            '--F_SIGF', 
                'fullPath=$CCP4I2/demo_data/gamma/merged_intensities_Xe.mtz',
            '--ASUFILE',
                'seqFile=$CCP4I2/demo_data/gamma/gamma.pir', 
            '--SFAC', 'Xe',
            '--NTRY', '4',
            '--PARTIALMODELORMAP', 'SEARCH',
            '--COMP', 'ASU',
            '--LLGC_CYCLES', '50',
            '--RUNPARROT', 'True',
            '--RUNBUCCANEER', 'True'
        ]
    ],

    'ProvideAsuContents':[
        ['ProvideAsuContents',
        '--ASU_CONTENT', 
            'description=Beta-lactamase', 
            'sequence=HPETLVKVKDAEDQLGARVGYIELDLNSGKILESFRPEERFPMMSTFKVLLCGAVLSRVDAGQEQLGRRIHYSQNDLVEYSPVTEKHLTDGMTVRELCSAAITMSDNTAANLLLTTIGGPKELTAFLHNMGDHVTRLDRWEPELNEAIPNDERDTTMPAAMATTLRKLLTGELLTLASRQQLIDWMEADKVAGPLLRSALPAGWFIADKSGAGERGSRGIIAALGPDGKPSRIVVIYTTGSQATMDERNRQIAEIGASLIKHW', 
            'source/baseName=beta.seq', 
            'source/relPath=$CCP4I2_ROOT/demo_data/beta_blip', 
            'nCopies=1', 
            'polymerType=PROTEIN', 
            'name=Beta-lactamase']
    ],
    'phaser_simple':[
        ['phaser_simple','--F_SIGF', 'fullPath=$CCP4I2/demo_data/gamma/merged_intensities_Xe.mtz',
         '--XYZIN', 'fullPath=$CCP4I2/demo_data/gamma/gamma_model.pdb',
         '--RUNREFMAC','False']
        ],
    'phaser_simple_refmac':[
        ['phaser_simple',
            '--F_SIGF', 'fullPath=$CCP4I2/demo_data/gamma/merged_intensities_Xe.mtz',
            '--XYZIN', 'fullPath=/$CCP4I2/demo_data/gamma/gamma_model.pdb',
            '--RUNREFMAC','False'
        ],
        ['prosmart_refmac',
            '--F_SIGF', 'fileUse=1.F_SIGF',
            '--XYZIN', 'fileUse=1.XYZOUT'
        ]
    ],
    'phaser_pipeline':[
        ['import_merged',
            '--HKLIN', 'fullPath=$CCP4I2/demo_data/beta_blip/beta_blip_P3221.mtz',
        ],
        ['phaser_pipeline',
            '--F_SIGF', 
                'fileUse=1.OBSOUT',
            '--ENSEMBLES', 
                'use=True', 
                'pdbItemList/identity_to_target=0.9',
                'pdbItemList/structure=$CCP4I2/demo_data/beta_blip/beta.pdb',
            '--ENSEMBLES', 
                'use=True', 
                'pdbItemList/identity_to_target=0.9',
                'pdbItemList/structure=$CCP4I2/demo_data/beta_blip/blip.pdb',
            '--RUNREFMAC','False',
            '--F_OR_I', 'F',
            '--RESOLUTION_HIGH', '3.0',
            '--COMP_BY', 'DEFAULT'
            ]
        ],

    'phaser_expert_asu':[
        ['import_merged',
            '--HKLIN', 'fullPath=$CCP4I2/demo_data/beta_blip/beta_blip_P3221.mtz',
        ],
        ['phaser_pipeline',
            '--F_SIGF', 
                'fileUse=1.OBSOUT',
            '--ENSEMBLES', 
                'use=True', 
                'pdbItemList/identity_to_target=0.9',
                'pdbItemList/structure=$CCP4I2/demo_data/beta_blip/beta.pdb',
            '--ENSEMBLES', 
                'use=True', 
                'pdbItemList/identity_to_target=0.9',
                'pdbItemList/structure=$CCP4I2/demo_data/beta_blip/blip.pdb',
            '--RUNREFMAC','False',
            '--F_OR_I', 'F',
            '--RESOLUTION_HIGH', '3.0',
            '--ASUFILE',
                'seqFile=$CCP4I2/demo_data/beta_blip/beta.seq',
                'seqFile=$CCP4I2/demo_data/beta_blip/blip.seq',
            ]
        ],
    'phaser_expert_asu_singlejob':[
        ['phaser_pipeline',
            '--F_SIGF', 
                'fullPath=$CCP4I2/demo_data/beta_blip/beta_blip_P3221.mtz',
                'columnLabels=/*/*/[Fobs,Sigma]',
            '--ENSEMBLES', 
                'use=True', 
                'pdbItemList/identity_to_target=0.9',
                'pdbItemList/structure=$CCP4I2/demo_data/beta_blip/beta.pdb',
            '--ENSEMBLES', 
                'use=True', 
                'pdbItemList/identity_to_target=0.9',
                'pdbItemList/structure=$CCP4I2/demo_data/beta_blip/blip.pdb',
            '--RUNREFMAC','False',
            '--F_OR_I', 'F',
            '--RESOLUTION_HIGH', '3.0',
            '--ASUFILE',
                'seqFile=$CCP4I2/demo_data/beta_blip/beta.seq',
                'seqFile=$CCP4I2/demo_data/beta_blip/blip.seq',
            ]
        ],
    'SubstituteLigand':[
        ['SubstituteLigand',
            '--UNMERGEDFILES', 
                'file=$CCP4I2/demo_data/mdm2/mdm2_unmerged.mtz',
                'crystalName=C1', 
                'dataset=D1',
         '--XYZIN', 
            'fullPath=$CCP4I2/demo_data/mdm2/4hg7.cif',  
            'selection/text=A/',
         '--LIGANDAS','SMILES',
         '--SMILESIN','"CC(C)OC1=C(C=CC(=C1)OC)C2=NC(C(N2C(=O)N3CCNC(=O)C3)C4=CC=C(C=C4)Cl)C5=CC=C(C=C5)Cl"','--PIPELINE','DIMPLE']
        ],
    'SubstituteLigand_Dimple':[
        ['SubstituteLigand',
            '--UNMERGEDFILES', 
                'file=$CCP4I2/demo_data/mdm2/mdm2_unmerged.mtz',
                'crystalName=C1', 
                'dataset=D1',
        '--PIPELINE', 'DIMPLE',
         '--XYZIN', 
            'fullPath=$CCP4I2/demo_data/mdm2/4hg7.cif',  
            'selection/text=A/',
         '--LIGANDAS','SMILES',
         '--SMILESIN','"CC(C)OC1=C(C=CC(=C1)OC)C2=NC(C(N2C(=O)N3CCNC(=O)C3)C4=CC=C(C=C4)Cl)C5=CC=C(C=C5)Cl"','--PIPELINE','DIMPLE']
        ],
    'SubstituteLigandPdbIn':[
        ['SubstituteLigand',
            '--UNMERGEDFILES', 
                'file=$CCP4I2/demo_data/mdm2/mdm2_unmerged.mtz',
            'crystalName=C1', 
            'dataset=D1',
         '--XYZIN', 
            'fullPath=$CCP4I2/demo_data/mdm2/4hg7.pdb',  
            'selection/text=A/',
         '--LIGANDAS','SMILES',
         '--SMILESIN','"CC(C)OC1=C(C=CC(=C1)OC)C2=NC(C(N2C(=O)N3CCNC(=O)C3)C4=CC=C(C=C4)Cl)C5=CC=C(C=C5)Cl"','--PIPELINE','DIMPLE']
        ],
    'parrot':[
        ['parrot',
         '--F_SIGF', 'fullPath=$CCP4I2/demo_data/gamma/merged_intensities_Xe.mtz',
         '--ABCD', 'fullPath=$CCP4I2/demo_data/gamma/initial_phases.mtz',
         '--ASUIN',
            'seqFile=$CCP4I2/demo_data/gamma/gamma.pir', 
            'selection/gamma=True',
        ]
        ],
    'shelxSAD':[
        ['import_merged',
            '--HKLIN', 
                'fullPath=$CCP4I2/demo_data/gamma/merged_intensities_Xe.mtz',
        ],
        ['shelx',  
            '--SHELXCDE', 'True', 
            '--ATOM_TYPE', 'Xe', 
            '--F_SIGFanom', 'fileUse=1.OBSOUT',
            '--SEQIN',
                'seqFile=$CCP4I2/demo_data/gamma/gamma.pir', 
            '--USER_MBREF_BIGCYC', 'True',
            '--MBREF_BIGCYC', '2'
        ]
        ],
    'aimless_gamma_native':[
        ['aimless_pipe',
            '--UNMERGEDFILES', 
                'crystalName=Nat1', 
                'dataset=DS1', 
                'file=$CCP4I2/demo_data/gamma/gamma_native.mtz',
        ],
    ],
    'shelxSIRAS':[
        ['import_merged',
            '--HKLIN', 
                'fullPath=$CCP4I2/demo_data/gamma/merged_intensities_Xe.mtz',
        ],
        ['aimless_pipe',
            '--UNMERGEDFILES', 
                'crystalName=Nat1', 
                'dataset=DS1', 
                'file=$CCP4I2/demo_data/gamma/gamma_native.mtz',
        ],
        ['shelx',  
            '--SHELXCDE', 'True', 
            '--ATOM_TYPE', 'Xe', 
            '--EXPTYPE', 'SIRAS',
            '--NATIVE', 'True',
            '--F_SIGFanom', 'fileUse="-2.OBSOUT"',
            '--F_SIGFnative', 'fileUse="-1.HKLOUT[0]"',
            '--SEQIN',
                'seqFile=$CCP4I2/demo_data/gamma/gamma.pir', 
            '--USER_MBREF_BIGCYC', 'True',
            '--MBREF_BIGCYC', '2'
        ]
        ],
    'phaser_expert_tncs':[
         ['ProvideAsuContents',
        '--ASU_CONTENT',
            'description=3bbz',
            'sequence=AGQKVMITKMITDSVANPQMKQAFEQRLAKASTEDALNDIKRDIIRSAI',
            'source/baseName=3bbz.fa',
            'nCopies=2',
            'polymerType=PROTEIN',
            'name=3bbz'],
        ['phaser_pipeline',
            '--F_SIGF',
                'fullPath=/Users/huw/ccp4test/CCP4i2Testing/TestData/3bbz.mtz',
                'columnLabels=/*/*/[I,SIGI]',
            '--RUNREFMAC','False',
            '--ENSEMBLES',
                'use=True',
                'number=2',
                'pdbItemList/identity_to_target=1.0',
                'pdbItemList/structure=/Users/huw/ccp4test/CCP4i2Testing/TestData/3bbz_helices.pdb',
            '--RUNREFMAC','False',
            '--F_OR_I', 'I',
            '--ASUFILE',
                'fileUse=1.ASUCONTENTFILE'
            ],
            ['phaser_pipeline',
            '--F_SIGF',
                'fullPath=/Users/huw/ccp4test/CCP4i2Testing/TestData/3bbz.mtz',
                'columnLabels=/*/*/[I,SIGI]',
            '--RUNREFMAC','False',
            '--ENSEMBLES',
                'use=True',
                'number=2',
                'pdbItemList/identity_to_target=1.0',
                'pdbItemList/structure=/Users/huw/ccp4test/CCP4i2Testing/TestData/3bbz_helices.pdb',
            '--RUNREFMAC','False',
            '--F_OR_I', 'I',
            '--ASUFILE',
                'fileUse=1.ASUCONTENTFILE',
            '--TNCS_USE', 'False',
            ]
        ],
        'phaser_simple_no_solution':[
        ['phaser_simple','--F_SIGF', 'fullPath=$CCP4I2/demo_data/gamma/merged_intensities_Xe.mtz',
         '--XYZIN', 'fullPath=$CCP4I2/demo_data/rnase/rnase_model.pdb',
         '--RESOLUTION_HIGH', '5.0',
         '--RUNREFMAC','False']
        ],
    }

def buildTestProjects(args):
    taskNames = args.test
    zipDir = args.zipDir
    if not os.path.exists(zipDir):
        os.mkdir(zipDir)
    projectsRoot = tempfile.mkdtemp(prefix='CCP4I2_PROJECTS')
    dbFileName = os.path.join(projectsRoot,'ProjectTestCases.sqlite')
    destDir = os.path

    try :
        CCP4I2 = os.environ['CCP4I2']
    except KeyError:
        try:
            CCP4I2 = os.environ['CCP4I2_TOP']
        except:
            try:
                CCP4I2 = os.path.join(os.environ['CCP4'], 'share', 'ccp4i2')
            except KeyError:
                raise Exception("Unable to identify CCP4I2 root: did you prepare CCP4 environment?")
    os.environ.setdefault('CCP4I2', CCP4I2)
    
    
    try :
        i2DotDir = os.environ['CCP4_LOCAL_DOTDIR']
    except KeyError:
        raise Exception("CCP4_LOCAL_DOTDIR must be set before running tests")

    if len(taskNames) == 0:
        taskTestCases = allTaskTestCases
    else:
        taskTestCases = {testCaseName:allTaskTestCases[testCaseName] for testCaseName in allTaskTestCases if testCaseName in taskNames}

    print ("Number of test cases is ", len(taskTestCases))

    sys.path.append(CCP4I2)
    from utils import startup
    from core import CCP4Config
    CCP4Config.CConfig(mode='BuildCCP4i2TestProjects').insts.graphical = False

    pm = startup.startProjectsManager(dbFileName=dbFileName)

    for testName in taskTestCases:
        for iTest, testCase in enumerate(taskTestCases[testName]):
            baseArgs  = [os.path.expandvars(os.path.join('$CCP4I2', 'bin', 'i2run')), testCase[0], '--dbFile', dbFileName]
            print ('testCase:', testCase)
            projectName = '{}_test_{}'.format(testName, 0)
            extendedArgs = baseArgs + ['--projectName', projectName,
                                       '--projectPath', os.path.join(projectsRoot, projectName)]
            
            for arg in testCase[1:]:
                extendedArgs.append(arg)
            print(extendedArgs)
            returnValue = subprocess.call(extendedArgs)
            if returnValue != 0:
                continue
            projectInfo = pm.db().getProjectInfo(projectName=projectName)
            fileName = os.path.join(zipDir, projectName+".ccp4_project.zip")
            pm.compressProject(projectInfo['projectid'], after=None, excludeI2files=False, fileName=fileName, blocking=True)
    print("Command to view test Project: {} -db {}".format(os.path.expandvars(os.path.join('$CCP4I2', 'bin', 'ccp4')), dbFileName))
    pm.db().close()
    #shutil.rmtree(projectsRoot)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-t','--test', nargs='+', help='Specify test to run', required=False, default=[])
    parser.add_argument('--zipDir', default=os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),'ProjectZips'))
    args = parser.parse_args()
    buildTestProjects(args)
