from __future__ import print_function
import os
import glob

def pytest_addoption(parser):
    testBaseDir = os.path.dirname(os.path.normpath(os.path.abspath(__file__)))
    rootDir = os.path.dirname(testBaseDir)
    projectZipsPath = os.path.join(rootDir, "ProjectZips")
    parser.addoption('--test', action='append', default=None,
                     help='Path to ccp4.project.zip of test to run')
    parser.addoption('--zipDir', default=projectZipsPath,
                     help='Directory in which test case.ccp4_project.zip files are stored')

def pytest_generate_tests(metafunc):
    if 'testProjectPath' in metafunc.fixturenames:
        if metafunc.config.option.test is not None:
            metafunc.parametrize('testProjectPath', metafunc.config.option.test)
        else:
            testProjectPaths = glob.glob(os.path.join(metafunc.config.option.zipDir, 
                                         "*.ccp4_project.zip"))
            metafunc.parametrize('testProjectPath', testProjectPaths)
                
