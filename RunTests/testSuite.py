import glob
import os
import re
import shutil
import subprocess
import sys
import tempfile
import warnings
import ccp4i2
import gemmi
from lxml import etree
try:
    from termcolor import colored
except ModuleNotFoundError:
    def colored(txt, col):
        return txt


# Set non-graphical mode in CCP4i2
if "CCP4I2" in os.environ:
    CCP4I2 = os.environ["CCP4I2"]
else:
    CCP4I2 = ccp4i2.__path__[0]
    os.environ["CCP4I2"] = CCP4I2
sys.path.insert(0, CCP4I2)
from core import CCP4Config

CCP4Config.CConfig().insts.graphical = False

# check CCP4_LOCAL_DOTDIR has been set
if "CCP4_LOCAL_DOTDIR" not in os.environ:
    raise EnvironmentError("CCP4_LOCAL_DOTDIR must be set before running tests")


# Full path to ccp4 binaries
if sys.platform.startswith("win"):
    testi2sys = os.path.join(CCP4I2, "bin", "testi2sys.bat")
else:
    testi2sys = os.path.join(CCP4I2, "bin", "testi2sys")
superpose = shutil.which("superpose")
cad = shutil.which("cad")
sftools = shutil.which("sftools")
truncate = shutil.which("truncate")
cphasematch = shutil.which("cphasematch")
csymmatch = shutil.which("csymmatch")
psymmate = shutil.which("phaser.find_alt_orig_sym_mate")


# @pytest.mark.integration   (allows tests to be run independently (setup in ini file)
def test_eval(testProjectPath):
    # Creating the test object runs i2
    i2test = TSuite(testProjectPath)
    i2test.HasItRun()
    i2test.AreResultsComp()


# Class defining test Object, runs i2 & associated tests.
class TSuite:

    def __init__(self, testProjectPath):
        self.jobinfo = []
        self.ErrorList = []
        # Note, the zipfile (names the dirs) & testName (used for basedir) may be different.
        self.testName = os.path.basename(testProjectPath).split(".")[0]
        self.tempDir = tempfile.mkdtemp(suffix=self.testName, prefix="tmp_")
        self.OriFolder = None
        self.RerFolder = None
        self.__runi2Project(testProjectPath)
        self.__xmltomem()

    def __runi2Project(self, testProjectPath):
        logdir = "logs"
        if not os.path.isdir(logdir):
            os.mkdir(logdir)
        outfn = self.testName + "_run.txt"
        fk = open(os.path.join(logdir, outfn), "w")
        subprocess.call(
            [testi2sys, "--xmlOut", "-o", self.tempDir, "-p", testProjectPath, "-i", "1"],
            stdout=fk,
        )
        fk.close()
        # Get path for test i2sys xml file
        xmlFilePaths = glob.glob(os.path.join(self.tempDir, "*.xml"))
        assert len(xmlFilePaths) == 1, "There should only be one i2 xml results file."
        self.xmlFilePath = xmlFilePaths[0]  # Path to the base xml runfile.
        # Get the original & the re-run folders
        folders = [
            f
            for f in glob.glob(os.path.join(self.tempDir, "*"))
            if os.path.isdir(f) and os.path.split(f)[1] != "tmp_dir"
        ]
        for folder in folders:
            if re.search(r"rerun", folder):
                self.oriFolder = folder
            else:
                self.rerFolder = folder
        # Some debug info
        # print("Folders ", self.oriFolder, self.rerFolder)
        # print("xml et al")
        # print("Test:", self.testName, ", output xml is:", self.xmlFilePath)
        # print(os.listdir(os.path.dirname(self.xmlFilePath)))

    def __xmltomem(self):
        # Does what it says in the label. Loads relevant xml details into the Errorlist.
        xmlTree = etree.parse(self.xmlFilePath)
        xmlRoot = xmlTree.getroot()
        projectsInTest = xmlRoot.xpath("/ProjectTesting/Project")
        assert (
            len(projectsInTest) == 1
        ), "There should only be one i2 project in the test."
        self.projectInTest = projectsInTest[0]
        self.jobsInProject = self.projectInTest.xpath("Job")
        assert (
            len(self.jobsInProject) >= 1
        ), "There should be one job in the i2 project; no more, no less."
        for jobInProject in self.jobsInProject:
            errorReportLists = self.__GetErrorReportList(jobInProject)
            assert len(errorReportLists) <= 1
            if len(errorReportLists) == 1:
                for errorReport in errorReportLists[0].xpath("errorReport"):
                    self.__loadErrorReport(errorReport)

    def __GetErrorReportList(self, jobInProject):
        # returns error list, does basic xml consistency checks, files basic job info.
        statusNodes = jobInProject.xpath("Status")
        expectedStatusNodes = jobInProject.xpath("ExpectedStatus")
        assert (
            len(statusNodes) == 1
        ), "Problem. More than one status node in i2 error-log (xml)."
        assert (
            len(expectedStatusNodes) == 1
        ), "Problem. More than one status node in i2 error-log (xml)"
        recinf = [
            jobInProject.get("number"),
            statusNodes[0].text,
            expectedStatusNodes[0].text,
        ]
        self.jobinfo.append(recinf)
        errorReportLists = jobInProject.xpath("errorReportList")
        return errorReportLists

    def __loadErrorReport(self, errorReport):
        # Loads the error report into container class (& appends to list).
        severityNodes = errorReport.xpath("severity")
        assert len(severityNodes) == 1
        classNameNodes = errorReport.xpath("className")
        assert len(classNameNodes) == 1
        codeNodes = errorReport.xpath("code")
        assert len(codeNodes) == 1
        descriptionNodes = errorReport.xpath("description")
        assert len(descriptionNodes) == 1
        detailNodes = errorReport.xpath("details")
        assert len(detailNodes) <= 1
        fullDesc = descriptionNodes[0].text
        if len(detailNodes) == 1:
            fullDesc += " " + detailNodes[0].text
        # Load error into container class & list. (need to revise this..cf job info)
        error = i2ErrInfo(
            "job_" + self.jobinfo[-1][0],
            classNameNodes[0].text,
            codeNodes[0].text,
            severityNodes[0].text,
            fullDesc,
        )
        self.ErrorList.append(error)

    def HasItRun(self):
        # This should cover basic failures in running the project.
        # Both the template & re-run jobs should have run to completion, otherwise triggers an assert.
        for job in self.jobinfo:
            # Check job complete status, as written out by i2
            assert len(job) == 3
            assert job[1] == job[2], (
                "Job "
                + job[0]
                + " has different completion status in test. Now "
                + job[1]
                + ". Originally was "
                + job[2]
            )
        print("Errors Reported by main ccp4i2 code (shown for information purposes)")
        for error in self.ErrorList:
            print(
                error.jobNum,
                error.i2class,
                colored(error.code, i2ErrInfo.colors.get(error.sev, "cyan")),
                error.i2type,
                error.desc,
            )
        for error in self.ErrorList:
            isPluginError = error.i2class == "CPluginScript" and error.sev == "ERROR"
            assert not (
                isPluginError
            ), "Potentially serious problem encountered running i2, check log"

    def AreResultsComp(self):
        ccheck = ConsisCheck(self.tempDir, self.oriFolder, self.rerFolder)
        for job in self.jobinfo:
            print("Review Job : ", job[0])
            ccheck.checkpdbs(job[0])
            try:  # Remove this once exe access fixed in the ccp4 bin folder
                ccheck.checkpsymmate(job[0])
            except:
                print(
                    "General Phaser sym mate issue: likely to be the non-executable ccp4 binary ! chmod needed."
                )
            ccheck.checkcsym(job[0])
            ccheck.checkphase(job[0])
            ccheck.checkRefl(job[0])

    def OriginalComp(self):
        # This replicates the original code. Not recommended for use due to the chksum.
        for error in self.ErrorList:
            assert (
                error.i2type == "OK"
                or error.i2type == "WARNING"
                or error.code == "301"
                or error.code == "308"
            )
            if error.code == "301" or error.i2type == "WARNING":
                warnings.warn(
                    UserWarning(
                        "{} - {} : {}".format(
                            classNameNodes[0].text,
                            codeNodes[0].text,
                            descriptionNodes[0].text,
                        )
                    )
                )


class i2ErrInfo:
    colors = {"OK": "green", "WARNING": "yellow", "ERROR": "red"}

    # Minimal container class for the xml error info.
    def __init__(self, jobNum, errClass, errCode, errStat, errDesc):
        self.jobNum = jobNum  # The job number which gave rise to the error
        self.i2class = errClass  # The original class where error code generated
        self.code = errCode  # The i2 code generated (careful with these !)
        self.i2type = errStat  # The i2 status code for the error
        self.desc = errDesc  # The error description saved by i2
        self.__classify()

    def __classify(self):
        self.sev = self.i2type
        if self.code == "308":  # Typically ignore these (checksums on data output)
            self.sev = "WARNING"


class ConsisCheck:
    superpose_cut = 0.85
    csymm_cut = 0.0  # Fail test & assert if less.
    csymm_warn = 0.4  # Warning (basically it's failed)
    cphase_cut = 60.0  # degrees
    mlad_cut = 1.6  # Phaser sym mate check
    rcorr_cut = 0.75  # Correlation cut for reflections

    # Check output files are consistent. This is NOT the original i2 checks btw.
    def __init__(self, basefold=None, orifold=None, rerfold=None):
        print("\n")
        print("Initialising Consistency Checks")
        print("ccp4i2 test job ran in folder ", basefold)
        assert not (basefold == None), "No base directory specified, needed."
        assert (
            orifold != None and rerfold != None
        ), "Need both run folders (original & re-run), to work."
        self.isHL = None  # are the phases in HL format or Phi/FOM.
        self.basefolder = basefold
        self.oriFolder = orifold
        self.rerFolder = rerfold
        self.skipo = []

    def __getInputRefl(self, jobnum=None):
        inpxml = os.path.join(
            self.rerFolder, "CCP4_JOBS", "job_" + jobnum, "input_params.xml"
        )
        xmlTree = etree.parse(inpxml)
        xmlRoot = xmlTree.getroot()
        rpath = xmlRoot.xpath("ccp4i2_body/inputData/F_SIGF/relPath")[0].text
        fsigf = xmlRoot.xpath("ccp4i2_body/inputData/F_SIGF/baseName")[0].text
        oriFilePath = os.path.join(self.oriFolder, rpath, fsigf)
        rerFilePath = os.path.join(self.rerFolder, rpath, fsigf)
        if os.path.isfile(rerFilePath):
            retfile = rerFilePath
        else:
            retfile = oriFilePath
        # This particular file can include just about anything.
        refltyp = self.__ReflTyp(retfile)
        # No good if file anomalous (can't find any way to convert this other than manually).
        # Look for other file with "MEAN", which complements the anom vals (this should be in there).
        if refltyp == "Fplus" or refltyp == "Iplus":
            findmeanf = os.path.splitext(retfile)[0] + "*FMEAN*"
            findmeani = os.path.splitext(retfile)[0] + "*IMEAN*"
            sefilesf = glob.glob(findmeanf)
            sefilesi = glob.glob(findmeani)
            if len(sefilesf) == 1:
                retfile = sefilesf[0]
                refltyp = self.__ReflTyp(retfile)
            elif len(sefilesi) == 1:
                retfile = sefilesi[0]
                refltyp = self.__ReflTyp(retfile)
            else:
                assert (
                    False
                ), "Could not find a reflection file with F or I, or more than one file"
                return None
        # Now, if this file is anything other than F need to covert it first
        if refltyp != "F":
            assert (
                refltyp is "F" or "I"
            ), "Mean I or F not there ? No idea how this happened"
            retfile = self.__convertRefl(retfile)
        return retfile

    def __ReflTyp(self, retfile):
        """Uses gemmi i/o to extract data type from mtz reflection file. Expects i2 labels."""
        gmtz = gemmi.read_mtz_file(retfile)
        refltypl = ["F", "I", "Fplus", "Iplus"]
        refltyp = None
        for typ in refltypl:
            tmptyp = gmtz.column_with_label(typ)
            if tmptyp is not None:
                refltyp = typ
                return refltyp
        return None

    def __convertRefl(self, retfile):
        """Use truncate to convert I/SIGI to F/SIGF"""
        convFile = os.path.join(self.basefolder, "trin.mtz")
        tmp_inf = open(os.path.join(self.basefolder, "trin.txt"), "w")
        tmp_inf.write("LABIN IMEAN=I SIGIMEAN=SIGI\n")
        tmp_inf.close()
        tmp_inf = open(os.path.join(self.basefolder, "trin.txt"), "r")
        tmp_outf = open(os.path.join(self.basefolder, "trout.txt"), "w")
        subprocess.call(
            [truncate, "hklin", retfile, "hklout", convFile],
            stdout=tmp_outf,
            stdin=tmp_inf,
        )
        tmp_outf.close()
        tmp_inf.close()
        return convFile
        pass

    def __getI2OutputFiles(self, jobnum=None, fext="*.pdb"):
        assert jobnum is not None, "Job number must be given to find the output files"
        """Specialised function to look for output in i2 test runs. Looks for files of 
        type fext & outputs list of files (original & rerun)"""
        folders = [
            f
            for f in glob.glob(os.path.join(self.basefolder, "*"))
            if os.path.isdir(f) and os.path.split(f)[1] != "tmp_dir"
        ]
        # There should only be two folders. The original & re-run folders. The tmp_dir is excluded above.
        assert (
            len(folders) == 2
        ), "While searching for output files encountered unexpected folders in tmp run area."
        for folder in folders:
            if re.search(r"rerun", folder):
                folori = folder
            else:
                folnew = folder
        if type(fext) == str:
            pdbfileso = sorted(
                glob.glob(os.path.join(folori, "CCP4_JOBS", "job_" + jobnum, fext))
            )
            pdbfilesn = sorted(
                glob.glob(os.path.join(folnew, "CCP4_JOBS", "job_" + jobnum, fext))
            )
        if type(fext) == list:
            pdbfileso = []
            pdbfilesn = []
            for et in fext:
                pdbfileso.extend(
                    sorted(
                        glob.glob(
                            os.path.join(folori, "CCP4_JOBS", "job_" + jobnum, et)
                        )
                    )
                )
                pdbfilesn.extend(
                    sorted(
                        glob.glob(
                            os.path.join(folnew, "CCP4_JOBS", "job_" + jobnum, et)
                        )
                    )
                )
        if len(pdbfileso) != len(pdbfilesn):
            warnmessage = "Warning: Number of output files does not match (between original and new test run), check"
            warnings.warn(warnmessage)
            pdbfilesso_dict = {os.path.basename(path): path for path in pdbfileso}
            pdbfilessn_dict = {os.path.basename(path): path for path in pdbfilesn}
            common_basenames = set(pdbfilesso_dict) & set(pdbfilessn_dict)
            pdbfileso = [pdbfilesso_dict[basename] for basename in common_basenames]
            pdbfilesn = [pdbfilessn_dict[basename] for basename in common_basenames]
        return pdbfileso, pdbfilesn

    def checkpdbs(self, jobnum=None):
        """Run superpose and then check if it passes quality cut"""
        self.skipo = []
        print("---------------------------------")
        print("Running Superpose for model check (job", jobnum, ")")
        # First need to locate the pdbs (get folders first).
        pdbfileso, pdbfilesn = self.__getI2OutputFiles(jobnum, "*.pdb")
        for pdbo, pdbn in zip(pdbfileso, pdbfilesn):
            ofilen = os.path.split(pdbn)[1] + "_sup_job_" + jobnum + ".out"
            tmp_outf = open(os.path.join(self.basefolder, ofilen), "w")
            subprocess.call([superpose, pdbo, pdbn], stdout=tmp_outf)
            tmp_outf.close()
            with open(os.path.join(self.basefolder, ofilen), "r") as offy:
                suout = offy.read()
                if re.search(r"Superposition was not achieved", suout) is not None:
                    print(
                        "CCP4 Superpose failed to work on the file comparison for :",
                        os.path.split(pdbo)[1],
                    )
                    print(
                        "If this model file does not contain a protein this is expected."
                    )
                    self.skipo.append(pdbo)
                else:
                    seares = re.findall(r"quality Q:\s*(\d+\.\d+)", suout)
                    if len(seares) == 1:
                        quality = float(seares[0])
                        assert self.superpose_cut < quality <= 1.0, (
                            "Superpose reported a low Quality "
                            "value when comparing models. This indicates "
                            "the model could be significantly different "
                            "in the re-run."
                        )
                        print(
                            "Superpose calculated Quality for model",
                            os.path.split(pdbo)[1],
                            "(",
                            os.path.split(pdbn)[1],
                            ") ",
                            "= ",
                            quality,
                        )
                    else:
                        print(
                            "Something has gone wrong, too many numbers flagged in regex"
                        )

    def checkpsymmate(self, jobnum=None):
        # KJS. Note. Too much code duplication going on here.
        print("---------------------------------")
        print("Running phaser sym mate for model check (job", jobnum, ")")
        pdbfileso, pdbfilesn = self.__getI2OutputFiles(jobnum, "*.pdb")
        for pdbo, pdbn in zip(pdbfileso, pdbfilesn):
            if pdbo in self.skipo:
                # Skip input that superpose id'd as non-protein
                continue
            ofilen = os.path.join(
                self.basefolder, os.path.split(pdbn)[1] + "_phsr_job_" + jobnum + ".out"
            )
            tmp_outf = open(ofilen, "w")
            # phaser.find_alt_orig_sym_mate moving.pdb=in.pdb fixed.pdb=ref.pdb
            subprocess.call(
                [psymmate, "moving.pdb=" + pdbo, "fixed.pdb=" + pdbn], stdout=tmp_outf
            )
            tmp_outf.close()
            with open(os.path.join(self.basefolder, ofilen), "r") as offy:
                csyout = offy.read()
                seanum = re.findall(r"MLAD:\s*(\d+\.\d+)", csyout)
                if len(seanum) >= 1:
                    mladl = []
                    for num in seanum:
                        mlad = float(num)
                        mladl.append(mlad)
                        assert mlad <= self.mlad_cut, (
                            "MLAD Cut failed on chain for phaser sym mate test."
                            "This indicates the model could be significantly"
                            "different in the re-run."
                        )
                    print(
                        "Max. MLAD (by chain) calculated for model",
                        os.path.split(pdbo)[1],
                        "= ",
                        max(mladl),
                    )
                else:
                    print("No MLAD score recorded for model.")

    def checkcsym(self, jobnum=None):
        """Run csymmatch and check if it passes chain score cut"""
        print("---------------------------------")
        print("Running csymmatch for model check (job", jobnum, ")")
        # First need to locate the pdbs (get folders first).
        pdbfileso, pdbfilesn = self.__getI2OutputFiles(jobnum, "*.pdb")
        for pdbo, pdbn in zip(pdbfileso, pdbfilesn):
            if pdbo in self.skipo:
                # Skip input that superpose id'd as non-protein
                continue
            ofilen = os.path.join(
                self.basefolder, os.path.split(pdbn)[1] + "_csym_job_" + jobnum + ".out"
            )
            ofilepd = os.path.join(
                self.basefolder, os.path.split(pdbn)[1] + "_csym_job_" + jobnum + ".pdb"
            )
            tmp_outf = open(ofilen, "w")
            # csymmatch -pdbin-ref filename -pdbin filename -pdbout filename -connectivity-radius radius/A -origin-hand [-stdin]
            subprocess.call(
                [
                    csymmatch,
                    "-pdbin-ref",
                    pdbo,
                    "-pdbin",
                    pdbn,
                    "-pdbout",
                    ofilepd,
                    "-origin-hand",
                ],
                stdout=tmp_outf,
            )
            tmp_outf.close()
            with open(os.path.join(self.basefolder, ofilen), "r") as offy:
                csyout = offy.read()
                seanum = re.findall(r"with normalised score:\s*(\d+\.\d+)", csyout)
                if len(seanum) >= 1:
                    scorel = []
                    warnmessage = (
                        "Csymmatch reported a low score "
                        "value when applying symmetry operations for one of"
                        "the chains. This indicates the model could be significantly"
                        "different in the re-run."
                    )
                    for num in seanum:
                        score = float(num)
                        scorel.append(score)
                        assert self.csymm_cut < score <= 1.0, warnmessage
                        if self.csymm_cut < score <= self.csymm_warn:
                            warnings.warn(warnmessage)
                    print(
                        "Lowest csymmatch calculated chain score for model",
                        os.path.split(pdbo)[1],
                        " :",
                        min(scorel),
                    )
                else:
                    print("Something has gone wrong, no csymmatch shift")

    def checkphase(self, jobnum=None):
        """Run cphasematch and check if cut on <phase angle> passes"""
        # First need to locate the mtzs (get folders first).
        checkfor = ["*ABCD*.mtz", "PHASEOUT*.mtz"]
        mtzfileso, mtzfilesn = self.__getI2OutputFiles(jobnum, checkfor)
        for mtzo, mtzn in zip(mtzfileso, mtzfilesn):
            tnm = os.path.splitext(os.path.basename(mtzn))[0]
            ofilen = tnm + "_CADmergeOR.out"
            pob = os.path.join(self.basefolder, tnm + "_CADmergeOR.mtz")
            self.__cadjscript(mtzo, mtzn)
            # cad hklin1 ABCDOUT_1.mtz hklin2 ABCDOUT_2.mtz hklout outfile.mtz << EOF blah blah EOF
            tmp_inf = open(os.path.join(self.basefolder, "tmpcadin.txt"), "r")
            tmp_outf = open(os.path.join(self.basefolder, ofilen), "w")
            subprocess.call(
                [cad, "hklin1", mtzo, "hklin2", mtzn, "hklout", pob],
                stdout=tmp_outf,
                stdin=tmp_inf,
            )
            tmp_inf.close()
            tmp_outf.close()
            if os.path.isfile(pob):
                mtzin = self.__getInputRefl(jobnum)
                tmp_inf = open(os.path.join(self.basefolder, "tmpcad2in.txt"), "w")
                tmp_inf.write("LABIN FILE_NUMBER 1 ALL\n")
                tmp_inf.write("LABIN FILE_NUMBER 2 ALL\n")
                tmp_inf.close()
                tmp_inf = open(os.path.join(self.basefolder, "tmpcad2in.txt"), "r")
                ofilen = tnm + "_CADmergeRef.out"
                pob2 = os.path.join(self.basefolder, tnm + "_CADmergeRef.mtz")
                tmp_outf = open(os.path.join(self.basefolder, ofilen), "w")
                # print ("Running over hkl files", pob, mtzin)
                subprocess.call(
                    [cad, "hklin1", pob, "hklin2", mtzin, "hklout", pob2],
                    stdout=tmp_outf,
                    stdin=tmp_inf,
                )
                tmp_inf.close()
                tmp_outf.close()
            #  ...needs fp & sigfp as well.
            if os.path.isfile(pob2):
                print("-------------------------------------")
                print("Running cphasematch for phasing check (job", jobnum, ")")
                ofilen = tnm + "_cphase.out"
                # cphasematch -mtzin filename -mtzout filename
                # Need to feed in expected phase type
                tmp_outf = open(os.path.join(self.basefolder, ofilen), "w")
                phmcmd = [cphasematch, "-mtzin", pob2, "-colin-fo", "*/*/[F,SIGF]"]
                # Add the original & new phases to the cmd line.
                orifile = 0
                newfile = 1
                phmcmd.extend(self.__getphmlopt(orifile))
                phmcmd.extend(self.__getphmlopt(newfile))
                subprocess.call(phmcmd, stdout=tmp_outf)
                tmp_outf.close()
                # Now get what's needed
                phoutfile = open(os.path.join(self.basefolder, ofilen), "r")
                linea = phoutfile.readline()
                while linea:
                    if "Overall statistics:" in linea:
                        phoutfile.readline()
                        numlin = phoutfile.readline()
                        numlist = re.findall(r"-?\d+\.?\d*", numlin)
                        dphi = abs(float(numlist[3]))
                        dphi1 = abs(float(numlist[4]))
                        dphi2 = abs(float(numlist[5]))
                        print(
                            "CPhasematch : <dphi> = ",
                            dphi,
                            " ",
                            dphi1,
                            " ",
                            dphi2,
                            "  (nwt  wt1  wt2)\n",
                        )  # returns degrees (last two scaled)
                        assert (
                            0.0 <= dphi1 <= self.cphase_cut
                        ), "CPhasematch : The average phase difference between runs exceeds 40 degrees"
                        break
                    linea = phoutfile.readline()

    def __setHL(self, mtzo, mtzn):
        """Set an array isHLl which determines if two phase files are HL or Phi/FOM"""
        # We expect 7 columns for HL coefficients, 5 for Phi/FOM
        nfHL = 7
        gmtzo = gemmi.read_mtz_file(mtzo)
        gmtzn = gemmi.read_mtz_file(mtzn)
        ncolso = len(gmtzo.columns)
        ncolsn = len(gmtzn.columns)
        if ncolso != ncolsn:
            warnings.warn(
                "Re-run phase types do not match original (ie. one is HL, the other Phi/FOM). \n"
                + mtzo
                + "\n"
                + mtzn,
                stacklevel=0,
            )
        self.isHLl = [ncolso // nfHL, ncolsn // nfHL]

    def __cadjscript(self, mtzo, mtzn):
        """Fill in the card file for merging the phase mtz files together (with cad)"""
        if __name__ != "__main__":  # set manually for unittest
            self.__setHL(mtzo, mtzn)
        tmp_inf = open(os.path.join(self.basefolder, "tmpcadin.txt"), "w")
        for iocls in ["input", "output"]:
            for i, isHL in enumerate(self.isHLl):
                inLin = self.__getcadjIOLin(filen=i, isHL=isHL, iotype=iocls)
                tmp_inf.write(inLin)
                if __name__ == "__main__":
                    print("CAD Script Line : ", inLin)
        tmp_inf.close()

    def __getcadjIOLin(self, filen=999, isHL=1, iotype="input"):
        """Return card line for output files. Code is very specific to __cadjscript"""
        endlines_in = [" E1=PHI E2=FOM\n", " E1=HLA E2=HLB E3=HLC E4=HLD\n"]
        endlines_out = [
            [" E1=PHIO E2=FOMO\n", " E1=PHIR E2=FOMR\n"],
            [
                " E1=HLAO E2=HLBO E3=HLCO E4=HLDO\n",
                " E1=HLAR E2=HLBR E3=HLCR E4=HLDR\n",
            ],
        ]
        assert (
            filen != 999
        ), "Incorrect use of function __getcadjIOLin, you must specify the right output file number 0=original, 1=re-run!"
        if iotype == "input":
            cadjIOLin = "LABIN FILE_NUMBER " + str(filen + 1) + endlines_in[isHL]
        elif iotype == "output":
            cadjIOLin = (
                "LABOUT FILE_NUMBER " + str(filen + 1) + endlines_out[isHL][filen]
            )
        else:
            assert "Incorrect use of function __getcadjIOLin, you must specify input or output"
        return cadjIOLin

    def __getphmlopt(self, filen=999):
        """Set cphasematch cmd line options. Code is specific to checkphase"""
        hl_or_pf = ["phifom-", "hl-"]
        hl_or_pf_cols = [
            ["*/*/[PHIO,FOMO]", "*/*/[PHIR,FOMR]"],
            ["*/*/[HLAO,HLBO,HLCO,HLDO]", "*/*/[HLAR,HLBR,HLCR,HLDR]"],
        ]
        isHL = self.isHLl[
            filen
        ]  # is the file HL or Phi/FOM (must run __setHL first - should be run by cadjscript before this!!)
        flagc = "-colin-" + hl_or_pf[isHL] + str(filen + 1)
        flago = hl_or_pf_cols[isHL][filen]
        return [flagc, flago]

    def checkRefl(self, jobnum=None):
        """Check the correlation between reflection data."""
        print("---------------------------------")
        print("Checking correlation for mtz reflection output (job", jobnum, ")")
        checkfor = ["*_obs.mtz"]
        mtzfileso, mtzfilesn = self.__getI2OutputFiles(jobnum, checkfor)
        for mtzo, mtzn in zip(mtzfileso, mtzfilesn):
            dtypo = self.__ReflTyp(mtzo)
            dtypn = self.__ReflTyp(mtzn)
            gomtzo = gemmi.read_mtz_file(mtzo)
            gomtzn = gemmi.read_mtz_file(mtzn)
            rdato = gomtzo.get_float(dtypo)
            rdatn = gomtzn.get_float(dtypn)
            c = rdato.calculate_correlation(rdatn)
            print("Correlation Results (coef) = ", c.coefficient())
            assert (
                c.coefficient() >= self.rcorr_cut
            ), "Correlation coeficiant for output reflection data is lower than would be expected, indicating significantly different data"
